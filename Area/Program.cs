﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area
{
    class Program
    {
        static void Main(string[] args)
        {
            Plot plot1 = new Plot(5, 10);
            Plot plot2 = new Plot(7, 3);

            Console.WriteLine(plot1.ToString());
            Console.WriteLine(plot2.ToString());
            Console.WriteLine("Area1 =" + plot1.getArea());
            Console.WriteLine("Area2 =" + plot2.getArea());
            Console.WriteLine("AreaSum_1_2 =" + (plot1 + plot2));

            Plot plot3= 2*plot1;
            Console.WriteLine(plot3);

            Console.WriteLine(plot3.ToString().CharCount('e'));
            Console.WriteLine(plot3.ToString().GetCharsIsNumber());

            Console.ReadKey();

        }
    }
    public static class StringExtension
    {
        public static int CharCount(this string str, char c)
        {
            int count = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    count++;
            }
            return count;
        }
        public static string GetCharsIsNumber(this string str)
        {
            string s = "";
            int x = 0;

            for (int i = 0; i < str.Length; i++)
            {

                try
                {
                    x = Convert.ToInt16(str[i].ToString());
                    s += str[i];
                }
                catch { }
            }
            return s;
        }
        
    }
}
