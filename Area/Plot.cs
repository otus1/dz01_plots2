﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area
{
    /* 
        Класс описывает участки. Eчастки все прямоугольной формы.
        Участки можно складывать, вычитать менять размеры.
        По участку можно получить площадь

        The class describes the plots. The plots are all rectangular in shape.
        Plots  can  be added, subtracted, resized.
        On the plot you can get the area.
     */
    public class Plot
    {
        private int _height;
        private int _width;
        public Plot (int height, int width)
        {
            _height = height;
            _width = width;
        }
        public int getArea()
        {
            return _height * _width;
        }

        public override string ToString()
        {
            return $"Plot: height={_height}; width={_width};";
        }

        public static int operator +(Plot p1, Plot p2)
        {
            return p1.getArea() + p2.getArea();
        }
        public static Plot operator *(int koef, Plot p)
        {
            return new Plot(koef * p._height, p._width);
        }
        public static Plot operator *( Plot p,int koef)
        {
            return koef*p;
        }
    }
}
